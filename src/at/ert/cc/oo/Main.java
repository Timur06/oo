package at.ert.cc.oo;

public class Main {
    public static void main(String[] args){
        int a = 7;

        Car c1 = new Car();
        c1.brand = "Audi";
        c1.fuelConsumption = 7;
        c1.serialNumber = "A7";
        c1.fuelAmount = 70;
        c1.fuelAmountMax = 70;

        Car c2 = new Car();
        c2.brand = "Mercedes";
        c2.fuelConsumption = 6;
        c2.serialNumber = "AMG";
        c2.fuelAmount = 20;
        c2.fuelAmountMax = 80;

        System.out.println(c2.fuelAmount);
        c2.drive();
        System.out.println(c2.fuelAmount);
        c1.break1();
        c2.turboBoost();
        c1.honk(3);
        c2.getRemainingRange();
        //D R Y


    }

}
