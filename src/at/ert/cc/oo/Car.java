package at.ert.cc.oo;

public class Car {
    //Instanz/Gedächtnisvariablen

    //dont do that later
    public int fuelConsumption;
    public int fuelAmount;
    public int fuelAmountMax;
    public int fuelPrecent;
    public int fuelRange;
    public String brand;
    public String serialNumber;
    private String color;

    //Methode
    public void drive(){
        this.fuelAmount = this.fuelAmount - fuelConsumption;
        System.out.println("I am driving");
    }

    public void break1(){
        System.out.println("Ich bremse");
    }

    public void turboBoost(){
        this.fuelPrecent = (this.fuelAmount*100) / this.fuelAmountMax;
        if(this.fuelPrecent<10){
            System.out.println("Not enough fuel to go to Superboost");
        }
        else{
            System.out.println("SuperBoostMode");
        }
    }

    public void honk(int amountOfRepetitions){
        for(int i = 0; i<amountOfRepetitions; i++){
            System.out.println("Tuuut");
        }
    }

    public void getRemainingRange(){
        this.fuelRange = this.fuelAmount / this.fuelConsumption;
        System.out.println("Sie könnten noch: " + fuelRange + "x mal fahren!");
    }
}
